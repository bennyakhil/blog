<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
				'address'          => 'test address at test city ',
				'mobile'          => '9605999875',
                'password'       => Hash::make('password'),//'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => null,
                'created_at'     => '2019-09-24 19:16:02',
                'updated_at'     => '2019-09-24 19:16:02',
            ],
        ];

        User::insert($users);
    }
}
